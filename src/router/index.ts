import HomeComponent from "@/views/Home/index.vue";
import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    component: HomeComponent,
  },
];

const router = createRouter({
  routes,
  history: createWebHashHistory(),
});

export { router };
