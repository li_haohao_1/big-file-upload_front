import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "path";

const pathResolve = (url: string): string => path.resolve(__dirname, url);
const port = 9222;

const SERVER_URL = "http://192.168.30.11:9334";
// const SERVER_URL = "http://localhost:9334";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  base: "/factory",
  resolve: {
    alias: {
      "@": pathResolve("src"),
    },
    extensions: [".vue", ".ts", ".tsx", ".js"],
  },
  server: {
    port,
    proxy: {
      "^/api": {
        target: SERVER_URL,
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
});
